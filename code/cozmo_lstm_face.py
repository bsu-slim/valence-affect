
# coding: utf-8

# In[1]:


import pandas as pd
import pickle
from tqdm import tqdm
import numpy as np


# In[2]:


data = pd.read_pickle('data/sentiment_data.pickle')
data['file_name'] = data.file_name.apply(lambda x: x.replace(' ', '_').split('.')[0])

emotions = ['interest','alarm','confusion','understanding','frustration','relief','sorrow','joy','anger','gratitude','fear','hope','boredom','surprise','disgust','desire']


from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import os
import ast


# ### Transfer learning from inceptionV3
# 
# - Taken from [here](https://blog.coast.ai/five-video-classification-methods-implemented-in-keras-and-tensorflow-99cad29cc0b5?gi=e8e7217e4d20), and [github](https://github.com/harvitronix/five-video-classification-methods)

# In[10]:
from attention import AttentionDecoder

from keras.preprocessing import image
from keras.applications.inception_v3 import InceptionV3, preprocess_input
from keras.models import Model, load_model
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Input
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping, CSVLogger
import time
import os.path

from keras.layers import Dense, Flatten, Dropout, ZeroPadding3D
from keras.layers.recurrent import LSTM
from keras.models import Sequential, load_model
from keras.optimizers import Adam, RMSprop
from keras.layers.wrappers import TimeDistributed
from keras.layers.convolutional import (Conv2D, MaxPooling3D, Conv3D,MaxPooling2D)
from collections import deque
import sys

import pickle


# In[11]:


base_model = InceptionV3(
                weights='imagenet',
                include_top=True
            )

            # We'll extract features at the final pool layer.
model = Model(
                inputs=base_model.input,
                outputs=base_model.get_layer('avg_pool').output
            )


# ### Only need to run this when extracting features from a keras model

# In[12]:


'''
face_folders = os.listdir('data/face_images')

for a in tqdm(face_folders):
    path =  'data/face_images/{}'.format(a)
    face_files = os.listdir(path)
    faces = []
    for f in face_files:
        f_path = 'data/face_images/{}/{}'.format(a,f)
        img = image.load_img(f_path, target_size=(299, 299))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0) 
        x = preprocess_input(x)
        features = model.predict(x)
        faces.append(features[0])
    with open('data/inceptionv3/{}.pkl'.format(a), 'wb') as f:
        pickle.dump(faces, f)
'''


# ### Train

# In[13]:


dev = pd.read_pickle('data/dev.pkl')
train = pd.read_pickle('data/train.pkl')


# In[14]:


START_COL = 'interest'
END_COL = 'desire'


def get_y(d):
    return d.loc[:,START_COL:END_COL].as_matrix()

def get_X(d):
    X = []
    for a in d.file_name:
        with open('data/inceptionv3/{}.pkl'.format(a), 'rb') as f:
            inc = pickle.load(f)
            X.append(inc)
    
    return np.array(X)


# In[15]:


y = get_y(train)
#y = y.reshape((1, y.shape[0], y.shape[1]))
X = get_X(train)
max_sequence = max([len(s) for s in X])
X = pad_sequences(X, maxlen=max_sequence, padding='post')
print(X.shape)
#X = X.reshape((1, X.shape[0], X.shape[1]))


#X = X.reshape((X.shape[0],X.shape[1],1))

test = dev

y_test = get_y(test)
#y_test = y_test.reshape((1,y_test.shape[0],y_test.shape[1]))
X_test = get_X(test)
X_test = pad_sequences(X_test, maxlen=max_sequence, padding='post')
#X_test = X_test.reshape((1,X_test.shape[0],X_test.shape[1]))


#X_test = X_test.reshape((X_test.shape[0], X_test.shape[1], 1))
#y.shape, X.shape, max_sequence


# In[16]:


checkpointer = ModelCheckpoint(
        filepath=os.path.join('data', 'checkpoints', 'lstm' + '-' +\
            '.{epoch:03d}-{val_loss:.3f}.hdf5'),
        verbose=1,
        save_best_only=True)

# Helper: TensorBoard
tb = TensorBoard(log_dir=os.path.join('data', 'logs', 'lstm'))

# Helper: Stop when we stop learning.
early_stopper = EarlyStopping(patience=25)

# Helper: Save results.
timestamp = time.time()
csv_logger = CSVLogger(os.path.join('data', 'logs', 'lstm' + '-' + 'training-' +     str(timestamp) + '.log'))


# In[17]:

print(X.shape, y.shape)
input_shape = (X.shape[1], X.shape[2])  # sequence length, features length
nb_classes = len(emotions)
batch_size = 64
nb_epoch = 1000


# In[18]:


model = Sequential()
model.add(LSTM(2048, return_sequences=False,
               input_shape=input_shape,
               dropout=0.5))
model.add(Dense(1024, activation='tanh'))
model.add(Dropout(0.5))
model.add(Dense(512, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(256,  activation='relu'))
#model.add(AttentionDecoder(32,X.shape[2]))
model.add(Dense(nb_classes, activation='softmax'))


# In[ ]:


metrics = ['accuracy']

optimizer = Adam(lr=1e-6, decay=1e-6)
model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                           metrics=metrics)

model.fit(
        X,
        y,
        batch_size=batch_size,
        validation_data=(X_test, y_test),
        verbose=1,
        callbacks=[tb, early_stopper, csv_logger],
        epochs=nb_epoch)

