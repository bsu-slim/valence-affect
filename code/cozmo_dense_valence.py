import pandas as pd
import pickle
from tqdm import tqdm
import numpy as np

from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import os
import ast
import json
import requests
from attention import AttentionDecoder

from keras.preprocessing import image
from keras.applications.inception_v3 import InceptionV3, preprocess_input
from keras.models import Model, load_model
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Input
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping, CSVLogger
import time
import os.path
import random
from keras.models import load_model
from keras.layers import BatchNormalization
from keras.layers import Dense, Flatten, Dropout, ZeroPadding3D
from keras.layers.recurrent import LSTM
from keras.models import Sequential, load_model
from keras.optimizers import Adam, RMSprop
from keras.layers.wrappers import TimeDistributed
from keras.layers.convolutional import (Conv2D, MaxPooling3D, Conv3D,MaxPooling2D)
from collections import deque
from sklearn.preprocessing import StandardScaler
import sys
import pickle
from keras import backend as K

def send_slack_message(msg):
    webhook_url = 'https://hooks.slack.com/services/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'
    slack_data = {'text': msg}

    response = requests.post(
        webhook_url, data=json.dumps(slack_data),
        headers={'Content-Type': 'application/json'}
    )



data = pd.read_pickle('../data/both_batches.pickle')
data['file_name'] = data.file_name.apply(lambda x: x.replace(' ', '_').split('.')[0])

emotions = data.columns[5:]
print(len(emotions), emotions)
emotions_dict = {i:e for i,e in enumerate(emotions)}

#emotions = ['interest','alarm','confusion','understanding','frustration','relief','sorrow','joy','anger','gratitude','fear','hope','boredom','surprise','disgust','desire']

positive_emotions = ['interest','understanding','relief',     'joy',   'gratitude','hope','surprise','desire']
negative_emotions = ['alarm',   'confusion',    'frustration','sorrow','anger',    'fear', 'boredom', 'disgust' ]


anim_files = [f.split('.')[0] for f in os.listdir('../data/processed_anims/')]

anims = {}

for a in tqdm(anim_files):
    path = '../data/processed_anims/{}.pkl'.format(a)
    with open(path, "rb" ) as f:
        anims[a] = pickle.load(f)

dev = pd.read_pickle('../data/dev.pkl')
test = pd.read_pickle('../data/test.pkl')
train = pd.read_pickle('../data/train.pkl')
#test = dev
test_type='test'

num_feats = len(anims[anim_files[0]][0]) 
print('feats', num_feats)
num_classes = len(emotions)
print('classes', num_classes)
max_len_anim = max([len(anims[x]) for x in anims]) 
print('max', max_len_anim) 
max_flat_anim = max_len_anim * num_feats

def get_X(d,max_sequence=None,test_train='train',c=None):
    X_d = []
    X_i = []
    for a in d.file_name:
        anim = anims[a]
        row = [list(x.values()) for x in anim]
        row = np.array([i for x in row for i in x]).astype('float32')
        X_i.append(row)

        with open('../data/padded_vgg19/{}.pkl'.format(a), 'rb') as f:
            inc = pickle.load(f)
            inc = np.array(inc[:50]) # first few frames are fairly the same across all of them
            X_d.append(inc.flatten()) 
    if max_sequence is None:
        max_sequence = max([len(s) for s in X_d])
    
    
    X_d = pad_sequences(X_d, maxlen=max_sequence, padding='post')
    X_i = pad_sequences(X_i, maxlen=max_flat_anim, padding='post')
    X_s = np.load('sound_model/{}_X.npy'.format(test_train))
    if c is not None: # for when one of two binary values is one
        c['X_s'] = X_s.tolist()
        X_s = c[c.index.isin(d.index)]['X_s'].values
        X_s = np.array([np.array(s) for s in X_s])
        c.drop(['X_s'],axis=1,inplace=True)
    X = np.concatenate((X_s,X_d,X_i),axis=1)
    
    return X,max_sequence


def get_model(nb_classes):
    model = Sequential()
    #model.add(LSTM(2048, return_sequences=True,
    #               input_shape=input_shape,
    #               dropout=0.5))
    act = 'tanh'
    model.add(Dense(2000, activation=act, input_dim=X.shape[1]))
    model.add(Dropout(0.5))    
    model.add(Dense(512, activation=act))
    #model.add(Dense(128, activation=act))
    # model.add(Flatten())
    model.add(Dense(nb_classes, activation='softmax'))
    metrics = ['acc']
    optimizer = Adam(lr=1e-5, decay=1e-6)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                            metrics=metrics)

    return model



for pos,neg in zip(positive_emotions, negative_emotions):
    #send_slack_message(str(('starting',pos,neg)))
    tmp_train = train[(train[pos]==1) | (train[neg]==1)]
    y = tmp_train[[pos,neg]].values

    X,ms = get_X(tmp_train,c=train)
    
    tmp_test = test[(test[pos]==1) | (test[neg]==1)]
    y_test = tmp_test[[pos,neg]].values
    X_test,_ = get_X(tmp_test,ms,test_type,c=test)

    checkpointer = ModelCheckpoint(
            filepath='model.{}-{}.h5'.format(pos,neg),
            verbose=1,
            monitor='val_acc',
            save_best_only=True)

    # Helper: TensorBoard
    tb = TensorBoard(log_dir=os.path.join('data', 'logs', 'lstm'))

    # Helper: Stop when we stop learning.
    early_stopper = EarlyStopping(patience=50)

    # Helper: Save results.
    timestamp = time.time()
    csv_logger = CSVLogger(os.path.join('data', 'logs', 'lstm' + '-' + 'training-' +     str(timestamp) + '.log'))
    print('training', pos, neg)
    input_shape = (None,X.shape[1])  # sequence length, features length
    input_shape = (X.shape[1],)
    nb_classes = 2
    batch_size = 32
    nb_epoch = 500
    model = get_model(nb_classes)
    
    model.fit(
            X,
            y,
            batch_size=batch_size,
            validation_data=(X_test, y_test),
            shuffle=True,
            verbose=1,
            callbacks=[tb, csv_logger, early_stopper, checkpointer],
            epochs=nb_epoch)
    
    #model.save('model.{}-{}.final.h5'.format(pos,neg)) 
    #del model
    #K.clear_session()
    print('evaluating')
    totals = np.sum(y_test,axis=0) / len(y_test)
    model = get_model(nb_classes)
    model.load_weights('model.{}-{}.h5'.format(pos,neg))
    scores = model.evaluate(X_test, y_test, verbose=0)
    out_str = "{}-{} ".format(pos,neg) + "%s: %.2f%%" % (model.metrics_names[1], scores[1]*100) + ' totals:{} {}'.format(totals, str(len(y_test)))
    print(out_str)
    send_slack_message(out_str)
    del model
    K.clear_session()
