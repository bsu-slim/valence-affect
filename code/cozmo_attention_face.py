
# coding: utf-8

# In[1]:


import pandas as pd
import pickle
from tqdm import tqdm
import numpy as np

import json
import requests
def send_slack_message(msg):
    webhook_url = 'https://hooks.slack.com/services/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'
    slack_data = {'text': msg}

    response = requests.post(
        webhook_url, data=json.dumps(slack_data),
        headers={'Content-Type': 'application/json'}
    )

# In[2]:


data = pd.read_pickle('../data/sentiment_data.pickle')
data['file_name'] = data.file_name.apply(lambda x: x.replace(' ', '_').split('.')[0])

emotions = ['interest','alarm','confusion','understanding','frustration','relief','sorrow','joy','anger','gratitude','fear','hope','boredom','surprise','disgust','desire']


from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import os
import ast


# ### Transfer learning from inceptionV3
# 
# - Taken from [here](https://blog.coast.ai/five-video-classification-methods-implemented-in-keras-and-tensorflow-99cad29cc0b5?gi=e8e7217e4d20), and [github](https://github.com/harvitronix/five-video-classification-methods)

# In[10]:
from attention import AttentionDecoder

from keras.preprocessing import image
from keras.applications.inception_v3 import InceptionV3, preprocess_input
from keras.models import Model, load_model
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Input
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping, CSVLogger
import time
import os.path
import random
from keras.layers import Dense, Flatten, Dropout, ZeroPadding3D
from keras.layers.recurrent import LSTM
from keras.models import Sequential, load_model
from keras.optimizers import Adam, RMSprop
from keras.layers.wrappers import TimeDistributed
from keras.layers.convolutional import (Conv2D, MaxPooling3D, Conv3D,MaxPooling2D)
from collections import deque
import sys

import pickle


# In[11]:


base_model = InceptionV3(
                weights='imagenet',
                include_top=True
            )

            # We'll extract features at the final pool layer.
model = Model(
                inputs=base_model.input,
                outputs=base_model.get_layer('avg_pool').output
            )


# ### Only need to run this when extracting features from a keras model

# In[12]:


'''
face_folders = os.listdir('../data/face_images')

for a in tqdm(face_folders):
    path =  '../data/face_images/{}'.format(a)
    face_files = os.listdir(path)
    faces = []
    for f in face_files:
        f_path = '../data/face_images/{}/{}'.format(a,f)
        img = image.load_img(f_path, target_size=(299, 299))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0) 
        x = preprocess_input(x)
        features = model.predict(x)
        faces.append(features[0])
    with open('../data/vgg19_padded/{}.pkl'.format(a), 'wb') as f:
        pickle.dump(faces, f)
'''


# ### Train

# In[13]:


dev = pd.read_pickle('../data/dev.pkl')
train = pd.read_pickle('../data/train.pkl')


# In[14]:


START_COL = 'interest'
END_COL = 'desire'


def get_y(d):
    return d.loc[:,START_COL:END_COL].as_matrix()

def get_X(d):
    X_d = []
    for a in d.file_name:
        with open('../data/padded_inceptionv3/{}.pkl'.format(a), 'rb') as f:
            inc = pickle.load(f)
            if len(inc) > 220: inc=inc[220:]
            X_d.append(inc)
    
    return np.array(X_d)


# In[15]:


y = get_y(train)
#y = y.reshape((y.shape[0],y.shape[1]))
X = get_X(train)
max_sequence = max([len(s) for s in X])
X = pad_sequences(X, maxlen=max_sequence, padding='post')
#X = X.reshape((1, X.shape[0], X.shape[1]))
print(X.shape, y.shape)


#X = X.reshape((X.shape[0],X.shape[1],1))


test = dev

y_test = get_y(test)
#y_test = y_test.reshape((y_test.shape[0],y_test.shape[1]))
X_test = get_X(test)
X_test = pad_sequences(X_test, maxlen=max_sequence, padding='post')
#X_test = X_test.reshape((1,X_test.shape[0],X_test.shape[1]))

#X = np.concatenate((X_test,X))
#y = np.concatenate((y_test,y))

#X_test = X_test.reshape((X_test.shape[0], X_test.shape[1], 1))
#y.shape, X.shape, max_sequence


# In[16]:


checkpointer = ModelCheckpoint(
        filepath=os.path.join('../data', 'checkpoints', 'lstm' + '-' +\
            '.{epoch:03d}-{val_loss:.3f}.hdf5'),
        verbose=1,
        save_best_only=True)

# Helper: TensorBoard
tb = TensorBoard(log_dir=os.path.join('data', 'logs', 'lstm'))

# Helper: Stop when we stop learning.
early_stopper = EarlyStopping(patience=100)

# Helper: Save results.
timestamp = time.time()
csv_logger = CSVLogger(os.path.join('data', 'logs', 'lstm' + '-' + 'training-' +     str(timestamp) + '.log'))


# In[17]:

input_shape = (X.shape[1], X.shape[2])  # sequence length, features length
nb_classes = len(emotions)
batch_size = 32
nb_epoch = 1000


# In[18]:


model = Sequential()
model.add(LSTM(64, return_sequences=True,
               input_shape=input_shape,
               dropout=0.5))
#model.add(Dense(128, activation='tanh'))
#model.add(Dropout(0.5))
model.add(AttentionDecoder(100,X.shape[2]))
model.add(Dense(32, activation='tanh'))
model.add(Flatten())
model.add(Dense(nb_classes, activation='softmax'))


# In[ ]:


metrics = ['acc']

optimizer = Adam(lr=0.001, decay=1e-6)
model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                           metrics=metrics)

model.fit(
        X,
        y,
        batch_size=batch_size,
        validation_data=(X_test, y_test),
        shuffle=True,
        verbose=1,
        callbacks=[tb, csv_logger],
        epochs=nb_epoch)

model.save('model.h5') 

#from keras.models import load_model
#model = load_model('model.internal-face.h5')

from sklearn.metrics import f1_score, accuracy_score

matches = []
guesses = []
golds = []
print('evaluating...')
for x,row in zip(X_test, test.iterrows()):
    i,row = row
    name = row['file_name']
    sub = test[test.file_name == name]
    gold = sub.loc[:,START_COL:END_COL].values
    gold = np.array([np.array(s) for s in gold])
    gold = list(np.sum(gold,axis=0))
    gold_t = [i for i,e in enumerate(gold) if e > 0]
    #print('summed',gold)
    x = x.reshape((1,x.shape[0],x.shape[1]))
    p = list(model.predict(x)[0])
    guess = p.index(max(p))
    guess_i = [1 if e > 0.1 else 0 for i,e in enumerate(p)]
    gold_i = [1 if e > 0 else 0 for i,e in enumerate(gold)]

    
    #print(gold_i, guess)
    matches.append(guess in gold_t)
    guesses.append(guess_i)
    golds.append(gold_i)
p = sum(matches) / len(matches)
#send_slack_message('cozmo {}'.format(str(p)))
print('acc', p)
avg_acc = np.mean([accuracy_score(gld, gs) for gld,gs in zip(golds, guesses)])
print('avg acc', avg_acc)
avg_f1 = np.mean([f1_score(gld, gs, average='macro') for gld,gs in zip(golds, guesses)])
print('f1',avg_f1 )




send_slack_message('cozmo acc {} avg acc {} f1 {}'.format(str(p),str(avg_acc),str(avg_f1)))
