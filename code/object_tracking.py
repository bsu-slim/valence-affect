# import the necessary packages

# from https://www.pyimagesearch.com/2015/09/14/ball-tracking-with-opencv/

from collections import deque
from imutils.video import VideoStream
import numpy as np
import argparse
import cv2
import imutils
import os
import time


# define the lower and upper boundaries of the "green"
# ball in the HSV color space, then initialize the
# list of tracked points
greenLower = (29, 86, 6)
greenUpper = (255, 255, 255)

 

def process_video(video, buffer = 64):

	pts = deque(maxlen=buffer)
	video = video.split('.')[0]
	#os.mkdir('../data/face_images/{}'.format(video))
	# otherwise, grab a reference to the video file
	vs = cv2.VideoCapture('../data/processed_videos/{}.mp4'.format(video))
	
	# allow the camera or video file to warm up
	time.sleep(2.0)

	# keep looping
	index = 0
	while True:
		# grab the current frame
		frame = vs.read()
		frame=frame[1]
		index += 1
	
		# if we are viewing a video and we did not grab a frame,
		# then we have reached the end of the video
		if frame is None:
			break
	
		# resize the frame, blur it, and convert it to the HSV
		# color space
		frame = imutils.resize(frame, width=600)
		
		blurred = cv2.GaussianBlur(frame, (11, 11), 0)
		hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
	
		# construct a mask for the color "green", then perform
		# a series of dilations and erosions to remove any small
		# blobs left in the mask
		mask = cv2.inRange(hsv, greenLower, greenUpper)
		mask = cv2.erode(mask, None, iterations=2)
		mask = cv2.dilate(mask, None, iterations=2)

		# find contours in the mask and initialize the current
		# (x, y) center of the ball
		cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
			cv2.CHAIN_APPROX_SIMPLE)
		cnts = imutils.grab_contours(cnts)
		center = None
	
		# only proceed if at least one contour was found
		if len(cnts) > 0:
			# find the largest contour in the mask, then use
			# it to compute the minimum enclosing circle and
			# centroid
			c = max(cnts, key=cv2.contourArea)
			#((x, y), radius) = cv2.minEnclosingCircle(c)
			M = cv2.moments(c)
			center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
			x,y,w,h = cv2.boundingRect(c)
			cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2)
			cv2.imwrite('data/face_images/{}/{}.png'.format(video,str(index)), frame[y:y+h, x:x+w])
			# only proceed if the radius meets a minimum size
			#if radius > 10:
				# draw the circle and centroid on the frame,
				# then update the list of tracked points
				#cv2.circle(frame, (int(x), int(y)), int(radius),
				#	(0, 255, 255), 2)
				#cv2.circle(frame, center, 5, (0, 0, 255), -1)
	
		# update the points queue
		pts.appendleft(center)

		# loop over the set of tracked points
		#for i in range(1, len(pts)):
			# if either of the tracked points are None, ignore
			# them
			#if pts[i - 1] is None or pts[i] is None:
			#	continue
	
			# otherwise, compute the thickness of the line and
			# draw the connecting lines
			#thickness = int(np.sqrt(buffer / float(i + 1)) * 2.5)
			#cv2.line(frame, pts[i - 1], pts[i], (0, 0, 255), thickness)


	
		# show the frame to our screen
		cv2.imshow("Frame", frame)
		key = cv2.waitKey(1) & 0xFF
	
		# if the 'q' key is pressed, stop the loop
		if key == ord("q"):
			break
	
	vs.release()
	
	# close all windows
	cv2.destroyAllWindows()


videos = os.listdir('../data/processed_videos/')

for video in videos:
	print(video)
	process_video(video)