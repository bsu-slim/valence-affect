
# coding: utf-8

# In[1]:


import pandas as pd
import pickle
from tqdm import tqdm_notebook as tqdm


# In[2]:


data = pd.read_pickle('data/sentiment_data.pickle')
data['file_name'] = data.file_name.apply(lambda x: x.replace(' ', '_').split('.')[0])

emotions = ['interest','alarm','confusion','understanding','frustration','relief','sorrow','joy','anger','gratitude','fear','hope','boredom','surprise','disgust','desire']


# In[3]:


data[:3]


# In[4]:


data.shape


# In[5]:


from collections import Counter as ctr


# In[6]:


tokens = [i for l in data.description.apply(lambda x: x.lower().split()) for i in l]


# In[7]:


ctr(tokens).most_common(50)


# In[8]:


emotions


# In[ ]:


for e in emotions:
    print(e, len(data[data[e] == 1]))


# In[10]:


import os
import ast


# In[11]:


anim_files = [f.split('.')[0] for f in os.listdir('data/processed_anims/')]

len(anim_files), anim_files[0]


# In[12]:


anims = {}

for a in tqdm(anim_files):
    path = 'data/processed_anims/{}.pkl'.format(a)
    with open(path, "rb" ) as f:
        anims[a] = pickle.load(f)


# In[13]:


len(anims[anim_files[0]])


# In[24]:


import numpy as np
from numpy import array
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.models import load_model
from pickle import load
from numpy import array
from numpy import argmax
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import load_model
from keras.utils.vis_utils import plot_model
from keras.models import Sequential
from keras import optimizers
from keras.layers import LSTM
from keras.layers import GlobalAveragePooling1D
from keras.layers import Dense
from keras.layers import Embedding
from keras.layers import RepeatVector
from keras.layers import Dropout
from keras.layers import TimeDistributed
from keras.layers import Conv1D
from keras.layers import InputLayer
from keras.layers import MaxPooling1D
from keras.callbacks import ModelCheckpoint
from keras.layers import Reshape
from sklearn.preprocessing import StandardScaler
from math import sqrt


# In[15]:


anim_col = [x['file_name'] for i,x in data.iterrows() if x['file_name'] in anims]

data = data[data.file_name.isin(anim_col)]


# In[16]:


num_feats = len(anims[anim_files[0]][0])
print('feats', num_feats)
num_classes = len(emotions)
print('classes', num_classes)
max_len_anim = max([len(anims[x]) for x in anims])
print('max', max_len_anim)
max_flat_anim = max_len_anim * num_feats


#this should only be run once! after that, load the pickles
test=data.sample(n=100,random_state=200)
test.to_pickle('data/test.pkl')
d=data.drop(test.index)
dev=d.sample(n=100, random_state=200)
dev.to_pickle('data/dev.pkl')
train=d.drop(dev.index)
train.to_pickle('data/train.pkl')

dev = pd.read_pickle('data/dev.pkl')
train = pd.read_pickle('data/train.pkl')

dev.shape, train.shape


# In[17]:


START_COL = 'interest'
END_COL = 'desire'


def get_y(d):
    return d.loc[:,START_COL:END_COL].as_matrix()

def get_X(d):
    X = []
    for a in d.file_name:
        anim = anims[a]
        row = [list(x.values()) for x in anim]
        row = np.array([i for x in row for i in x]).astype('float32')
        X.append(row)
        
    X = pad_sequences(X, maxlen=max_flat_anim, padding='post')
    
    return np.array(X)

'''
def get_X(d):
    X = []
    for a in d.file_name:
        anim = anims[a]
        row = np.array([list(x.values()) for x in anim])
        row = pad_sequences(row, maxlen=max_len_anim, padding='post').astype('float32')
        X.append(row)
    return np.array(X)
'''


# In[28]:


y = get_y(train)
X = get_X(train)

scaler = StandardScaler()
scaler = scaler.fit(X)
X = scaler.transform(X)

X = X.reshape((X.shape[0],X.shape[1],1))

test = dev

y_test = get_y(test)
X_test = get_X(test)
X_test = scaler.transform(X_test)
X_test = X_test.reshape((X_test.shape[0], X_test.shape[1], 1))

y.shape, X.shape


# In[33]:


model = Sequential()
model.add(LSTM(num_feats, input_shape=(X.shape[1],1)))
model.add(Dense(num_classes, activation='relu'))
# compile model

sgd = optimizers.SGD(lr=0.01, clipnorm=1.)
adag = optimizers.Adagrad(lr=0.01, epsilon=None, decay=0.0)
adam = optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)


model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['mse', 'accuracy'])
print(model.summary())


# In[ ]:


filename = 'model.h5'
checkpoint = ModelCheckpoint(filename, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
model.fit(X, y, epochs=20, batch_size=16, validation_data=(X_test,y_test), callbacks=[checkpoint], verbose=2)


# In[ ]:


model.evaluate(X_test, y_test)

