
# coding: utf-8

# In[1]:


import pandas as pd
import pickle
from tqdm import tqdm as tqdm
import numpy as np
from wac import WAC

from keras.preprocessing import image
from keras.applications.inception_v3 import InceptionV3, preprocess_input
from keras.models import Model, load_model
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Input


# In[2]:


data = pd.read_pickle('data/sentiment_data.pickle')
data['file_name'] = data.file_name.apply(lambda x: x.replace(' ', '_').split('.')[0])

emotions = ['interest','alarm','confusion','understanding','frustration','relief','sorrow','joy','anger','gratitude','fear','hope','boredom','surprise','disgust','desire']

emotions_ind = {i:e for i,e in enumerate(emotions)}

max_frames = []
for e in emotions:
    l = len(data[data[e] == 1])
    max_frames.append(l)
    print(e, l)
    
max_frames = max(max_frames)


import os
import ast


dev = pd.read_pickle('data/dev.pkl')
train = pd.read_pickle('data/train.pkl')

for e in emotions:
    print(e,len(train[train[e] == 1]),len(dev[dev[e] == 1]))

START_COL = 'interest'
END_COL = 'desire'


def get_y(d):
    return d.loc[:,START_COL:END_COL].as_matrix()

def get_X(d):
    X = []
    for a in d.file_name:
        with open('data/padded_vgg19/{}.pkl'.format(a), 'rb') as f:
            inc = pickle.load(f)
            X.append(inc)
    
    return np.array(X)


y = get_y(train)
X = get_X(train)
print(len(X))

max_sequence = max([len(s) for s in X])

test = dev

y_test = get_y(test)
X_test = get_X(test)

#X_test = pad_sequences(X_test, maxlen=max_sequence, padding='post')

#X_test = X_test.reshape((X_test.shape[0], X_test.shape[1], 1))

import random
#random.seed(a=200)
from sklearn import tree
from sklearn import neural_network
from sklearn import linear_model
from sklearn import gaussian_process
from sklearn import naive_bayes
from sklearn import ensemble
from wac import WAC

mlp_spec = (neural_network.MLPClassifier, {'hidden_layer_sizes':(128,64,32), 'solver':'adam', 'max_iter':1000, 'alpha':1e-05})
tree_spec = (tree.DecisionTreeClassifier,{'criterion':'gini'})
lr_spec=(linear_model.LogisticRegression,{'penalty':'l1'})
rf_spec = (ensemble.RandomForestClassifier, {'n_estimators':10, 'max_depth':2})
gauss_spec = (gaussian_process.GaussianProcessClassifier, {})
nb_spec= (naive_bayes.GaussianNB, {})


# In[54]:


wac = WAC('cozmo', mlp_spec)
    
X_y = list(zip(X,y))

for x,y_t in X_y:
    ind = np.argmax(y_t, axis=0)
    ind = emotions_ind[ind]
    for x_t in x:
        wac.add_observation(ind, x_t, [1])
        for i in range(5) : # add negative observations
            neg_X,neg_y = random.choice(X_y)
            neg_X = random.choice(neg_X)
            neg_y = np.argmax(neg_y, axis=0)
            neg_ind  = emotions_ind[neg_y]
            while neg_ind == ind:
                neg_X,neg_y = random.choice(X_y)
                neg_X = random.choice(neg_X)
                neg_y = np.argmax(neg_y, axis=0)
                neg_ind  = emotions_ind[neg_y]
            wac.add_observation(ind, neg_X, [0])    


# In[55]:


wac.train()


from operator import itemgetter

guesses= []
golds = []

X_y_test = list(zip(X_test, y_test))

for x,y_t in X_y_test:
    wac.new_utt()
    for x_t in x:
        context = (['i'],[x_t])
        predictions = [(e,wac.proba(e,context)[0][1]) for e in emotions]
        wac.compose(predictions)
        #guess = max(predictions,key=itemgetter(1))[0]
    guess = wac.get_predicted_intent()
    guesses.append(guess[0])
    golds.append(emotions_ind[np.argmax(y_t, axis=0)])

#list(zip(guesses, golds))


# In[58]:


from sklearn.metrics import accuracy_score

print(accuracy_score(guesses, golds))


