
# coding: utf-8

# In[1]:


import pandas as pd
import pickle
from tqdm import tqdm
import numpy as np


# In[2]:


data = pd.read_pickle('../data/both_batches.pickle')
data['file_name'] = data.file_name.apply(lambda x: x.replace(' ', '_').split('.')[0])

emotions = ['interest','alarm','confusion','understanding','frustration','relief','sorrow','joy','anger','gratitude','fear','hope','boredom','surprise','disgust','desire']


from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import os
import ast

import json
import requests
def send_slack_message(msg):
    webhook_url = 'https://hooks.slack.com/services/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'
    slack_data = {'text': msg}

    response = requests.post(
        webhook_url, data=json.dumps(slack_data),
        headers={'Content-Type': 'application/json'}
    )


# ### Transfer learning from inceptionV3
# 
# - Taken from [here](https://blog.coast.ai/five-video-classification-methods-implemented-in-keras-and-tensorflow-99cad29cc0b5?gi=e8e7217e4d20), and [github](https://github.com/harvitronix/five-video-classification-methods)

# In[10]:
from attention import AttentionDecoder

from keras.preprocessing import image
from keras.applications.inception_v3 import InceptionV3, preprocess_input
from keras.models import Model, load_model
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Input
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping, CSVLogger
import time
import os.path
import random
from keras.layers import BatchNormalization
from keras.layers import Dense, Flatten, Dropout, ZeroPadding3D
from keras.layers.recurrent import LSTM
from keras.models import Sequential, load_model
from keras.optimizers import Adam, RMSprop
from keras.layers.wrappers import TimeDistributed
from keras.layers.convolutional import (Conv2D, MaxPooling3D, Conv3D,MaxPooling2D)
from collections import deque
from sklearn.preprocessing import StandardScaler
import sys

import pickle


# In[11]:


base_model = InceptionV3(
                weights='imagenet',
                include_top=True
            )

            # We'll extract features at the final pool layer.
model = Model(
                inputs=base_model.input,
                outputs=base_model.get_layer('avg_pool').output
            )


# ### Only need to run this when extracting features from a keras model

# In[12]:


'''
face_folders = os.listdir('data/face_images')

for a in tqdm(face_folders):
    path =  'data/face_images/{}'.format(a)
    face_files = os.listdir(path)
    faces = []
    for f in face_files:
        f_path = 'data/face_images/{}/{}'.format(a,f)
        img = image.load_img(f_path, target_size=(299, 299))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0) 
        x = preprocess_input(x)
        features = model.predict(x)
        faces.append(features[0])
    with open('data/inceptionv3/{}.pkl'.format(a), 'wb') as f:
        pickle.dump(faces, f)
'''

anim_files = [f.split('.')[0] for f in os.listdir('../data/processed_anims/')]

anims = {}

for a in tqdm(anim_files):
    path = '../data/processed_anims/{}.pkl'.format(a)
    with open(path, "rb" ) as f:
        anims[a] = pickle.load(f)

features = {}
total = 0

# loop through the animations, gather all feature values
for anim in anims:
    for a in anims[anim]:
        total += 1
        for feat in a:
            if feat not in features: features[feat] = []
            features[feat].append(a[feat])
            
# for features that are all the same (i.e., std = 0), mark for removal
to_remove = []
#print(total)
for i in features:
    #print(i, np.mean(features[i]), np.std(features[i]))
    if np.std(features[i]) == 0:
        to_remove.append(i)
        
# remove all useless features
for anim in anims:
    new_anim = []
    for a in anims[anim]:
        new_a = {}
        for i in a:
            if i not in to_remove:
                new_a[i] = a[i]
        new_anim.append(new_a)
    anims[anim] = new_anim
        
        
anim_col = [x['file_name'] for i,x in data.iterrows() if x['file_name'] in anims]

data = data[data.file_name.isin(anim_col)]
# ### Train

# In[13]:


dev = pd.read_pickle('../data/dev.pkl')
test = pd.read_pickle('../data/test.pkl')
train = pd.read_pickle('../data/train.pkl')


# In[14]:

num_feats = len(anims[anim_files[0]][0]) 
print('feats', num_feats)
num_classes = len(emotions)
print('classes', num_classes)
max_len_anim = max([len(anims[x]) for x in anims]) 
print('max', max_len_anim) 
max_flat_anim = max_len_anim * num_feats

START_COL = 'interest'
END_COL = 'desire'


def get_y(d):
    return d.loc[:,START_COL:END_COL].values

def get_X(d,max_sequence=None):
    X_d = []
    X_i = []
    for a in d.file_name:
        anim = anims[a]
        row = [list(x.values()) for x in anim]
        row = np.array([i for x in row for i in x]).astype('float32')
        X_i.append(row)

        with open('../data/padded_vgg19/{}.pkl'.format(a), 'rb') as f:
            inc = pickle.load(f)
            #if len(inc) > 220: inc=inc[220:]
            inc = np.array(inc[:25]) # first few frames are fairly the same across all of them
            X_d.append(inc.flatten()) 
    if max_sequence is None:
        max_sequence = max([len(s) for s in X_d])
    
    
    X_d = pad_sequences(X_d, maxlen=max_sequence, padding='post')
    X_i = pad_sequences(X_i, maxlen=max_flat_anim, padding='post')
    # print('X_s', X_s.shape, X_d.shape, X_i.shape)
    X_d = np.concatenate((X_d, X_i),axis=1)
    ####X_d = X_d.reshape((1,X_d.shape[0], X_d.shape[1]))
    return X_d,max_sequence


# In[15]:


y = get_y(train)
#print(y)
#y = y / y.sum(axis=1)[:,None]
print(y)
#y = y.reshape((1,y.shape[0],y.shape[1]))
X,ms = get_X(train)

X_s = np.load('sound_model/train_X.npy')
X = np.concatenate((X_s,X),axis=1)
#X = X.reshape((1, X.shape[0], X.shape[1]))
print(X.shape, y.shape, ms)

scalers = {}
#print(X.shape[2]-10)
#for i in range(0,X.shape[1]):
#    scalers[i] = StandardScaler()
#    X[:, i, :] = scalers[i].fit_transform(X[:, i, :]) 

#X = X.reshape((X.shape[0],X.shape[1],1))


test = dev

y_test = get_y(test)
#y_test = y_test.reshape((1,y_test.shape[0],y_test.shape[1]))
X_test,_ = get_X(test,ms)
X_s = np.load('sound_model/dev_X.npy') # test = dev here
X_test = np.concatenate((X_s,X_test),axis=1)
# X_test = pad_sequences(X_test, maxlen=max_sequence, padding='post')
#for i in range(X_test.shape[1]):
#    X_test[:, i, :] = scalers[i].transform(X_test[:, i, :]) 

#X_test = scaler.transform(X)
#X_test = X_test.reshape((1,X_test.shape[0],X_test.shape[1]))

#X = np.concatenate((X_test,X))
#y = np.concatenate((y_test,y))

#X_test = X_test.reshape((X_test.shape[0], X_test.shape[1], 1))
#y.shape, X.shape, max_sequence


# In[16]:


checkpointer = ModelCheckpoint(
        filepath='model.h5',
        verbose=1,
        save_best_only=True)

# Helper: TensorBoard
tb = TensorBoard(log_dir=os.path.join('data', 'logs', 'lstm'))

# Helper: Stop when we stop learning.
early_stopper = EarlyStopping(patience=100)

# Helper: Save results.
timestamp = time.time()
csv_logger = CSVLogger(os.path.join('data', 'logs', 'lstm' + '-' + 'training-' +     str(timestamp) + '.log'))


# In[17]:

input_shape = (None,X.shape[1])  # sequence length, features length
input_shape = (X.shape[1],)
nb_classes = len(emotions)
batch_size = 32
nb_epoch = 1000


# In[18]:


model = Sequential()
#model.add(LSTM(2048, return_sequences=True,
#               input_shape=input_shape,
#               dropout=0.5))
act = 'tanh'
model.add(Dense(2500, activation=act, input_dim=X.shape[1]))
#model.add(Dropout(0.5))
#model.add(Dense(256, activation=act))
#model.add(Dropout(0.5))
#model.add(Dense(2048, activation=act))
#model.add(Dropout(0.5))
#model.add(AttentionDecoder(100,X.shape[2]))
#model.add(Dense(512, activation=act))
#model.add(Dropout(0.5))
#model.add(Dense(256, activation=act))
#model.add(Dropout(0.5))
#model.add(Dense(32, activation=act))
# model.add(Flatten())
model.add(Dense(nb_classes, activation='softmax'))


# In[ ]:


metrics = ['acc']

optimizer = Adam(lr=1e-5, decay=1e-6)
model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                           metrics=metrics)

model.fit(
        X,
        y,
        batch_size=batch_size,
        validation_data=(X_test, y_test),
        shuffle=True,
        verbose=0,
        callbacks=[tb, csv_logger],
        epochs=nb_epoch)


model.save('model.h5') 
#from keras.models import load_model
#model = load_model('model.h5')

matches = []
guesses = []
print('evaluating...')
for x,row in tqdm(zip(X_test, test.iterrows())):
    i,row = row
    name = row['file_name']
    sub = test[test.file_name == name]
    gold = sub.loc[:,START_COL:END_COL].values
    gold = np.array([np.array(s) for s in gold])
    gold = list(np.sum(gold,axis=0))
    #print('summed',gold)
    x = x.reshape((1,x.shape[0]))
    p = list(model.predict(x)[0])
    guess = p.index(max(p))
    gold_i = [i for i,e in enumerate(gold) if e > 0]
    #print(gold_i, guess)
    matches.append(guess in gold_i)
    guesses.append(guess)
p = sum(matches) / len(matches)
#send_slack_message('cozmo {}'.format(str(p)))
print(p)
print(guesses)

#with open('face_internal.pkl', 'wb') as f:
#    pickle.dump(guesses,f)
